// Sychronous programming - only one statement is being processed at a time
/*
    Error checking proves the synchronous programming.
    After detecting the error, the following line will not be processed.
*/

// console.log("Hello World 1")
// console.log("Hello World 2")
// console.log("Hello World 3")

// When certain statements takes a lot of time to process, this slows down the running/execution of code
// When a certain action takes a lot of time, this results in code blocking
// Code blocking - delaying of a more efficient code compared to ones currently executed
console.log("Hello World")
// for (let i = 0; i <= 1500; i++)
//     console.log(i)

console.log("Hello World Again")

// Fetch function with Fetch API - allows us to aynchronously fetch/request for a resource
// Promise is an object that represents eventual completion/failure of an asynchronous function and its result
// Syntax: fetch(URL) 

console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

// Retrieves all post following the REST method (GET)
// By using the then method, we can now check the status of a promise
// fetch method returns a Promise object
// then method catches the Promise object and returns another Promise which is either rejected/resolved
// Promise object handles asynchronous code
fetch("https://jsonplaceholder.typicode.com/posts").then(res => { console.log(res) })
console.log("Hello World")

// Using then method multiple times creates a promise chains
// json method converts the response object into JSON format to be used by application
fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json())
.then(json => console.log(json))

// async-await is another approach for JS asynchronous programming
// used in function to indicate which part of code should be waited.
// The code outside the function will be executed under JS asychronous
async function fetchData() {
    // Waits for the fetch method to be done before storing value of the response to result variable
    const result = await fetch("https://jsonplaceholder.typicode.com/posts")
    console.log(result)
    console.log(typeof result)

    const json = await result.json()
    console.log(json)
}

fetchData()
console.log("Hello Johnny")

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())
.then(json => console.log(json[0]))

// Using POST method to create objects (posts/:id)
fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            userID: 1,
            title: "New Post",
            body: "Hello World"
        }
    )
}).then(res => console.log(res.json()))

// Using PUT method to create objects (posts/:id)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            title: "Corrected Post",
        }
    )
}).then(res => console.log(res.json()))

// PUT replaces the whole object
// PATCH updates the specified key

fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            userID: 1,
            title: "Corrected Post",
        }
    )
}).then(res => console.log(res.json()))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE",
})

// Filtering Objects
// Syntax: url?key=val
// Syntax: url?key1=val1&key2=val2
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(res => res.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts?userId=1&id=3")
.then(res => res.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(res => res.json())
.then(json => console.log(json))