// Retrieve seperately all the task and their correspoding title
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(todo => {
    const title = todo.map(task => task.title)
    console.log(title)
    console.log(todo)
})

// Retrieve a single task and then displays its status and title
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(todo => { 
    console.log(`Title: ${todo.title}`)
    console.log(`Status: ${todo.status ? "Completed" : "Incomplete"}`) 
})

// Create a new task
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            userId: 1,
            title: "adipisci quia provident",
            completed: false
        }
    )
}).then(res => console.log(res.json()))

// Update the existing task using PUT method
fetch("https://jsonplaceholder.typicode.com/todos/2", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            title: "Updated Task Using PUT",
            description: "About REST API",
            status: true,
            dateComplete: "20/04/2022",
            userId: 1
        }
    )
}).then(res => console.log(res.json()))

// Update the existing task using PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/3", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(
        {
            status: true,
            dateComplete: "20/04/2022"
        }
    )
}).then(res => console.log(res.json()))

// Delete the existing task
fetch("https://jsonplaceholder.typicode.com/todos/4", { method: "DELETE" })